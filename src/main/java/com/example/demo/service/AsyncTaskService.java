package com.example.demo.service;

import org.springframework.scheduling.annotation.Async;

import java.util.Map;
import java.util.concurrent.Future;

public interface AsyncTaskService {

    @Async
    void executeAsyncTask(Map<String,Object> map);

    @Async
    Future<String> asyncInvokeReturnFuture(Map<String,Object> map);
}
