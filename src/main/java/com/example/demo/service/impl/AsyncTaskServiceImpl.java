package com.example.demo.service.impl;

import com.example.demo.observer.Observable;
import com.example.demo.service.AsyncTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

@Service
public class AsyncTaskServiceImpl implements AsyncTaskService {

    private static final Logger logger = LoggerFactory.getLogger(AsyncTaskServiceImpl.class);

    @Autowired
    private Observable observable;


    /**
     *
     * @param map
     */
    @Override
    public void executeAsyncTask(Map<String, Object> map) {
        logger.info("执行异步任务  ：" + Thread.currentThread().getName() +"  :  "+ map.get("value"));
        observable.setObj(new HashMap<>());
    }

    /**
     * 异常调用返回Future
     * @param map
     * @return
     */
    @Override
    public Future<String> asyncInvokeReturnFuture(Map<String, Object> map) {
        Future<String> future = new AsyncResult<String>("success:" );// Future接收返回值，这里是String类型，可以指明其他类型
        return future;
    }
}
