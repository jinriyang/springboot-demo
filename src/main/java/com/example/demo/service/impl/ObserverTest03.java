package com.example.demo.service.impl;

import com.example.demo.observer.Observable;
import com.example.demo.service.ObserverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ObserverTest03 implements ObserverService {

    private static final Logger logger = LoggerFactory.getLogger(ObserverTest03.class);

    public ObserverTest03(Observable buildObservable) {
        buildObservable.addObserver(this);
    }

    @Override
    public void update(Object arg) {

        logger.info(Thread.currentThread().getName() + " : 我是观察者 ObserverTest03 ");

    }
}
