package com.example.demo.service;

import org.springframework.scheduling.annotation.Async;

/**
 * @author jinriyang  2018-01-12 9:51
 */
public interface ObserverService {

    @Async
    void update(Object arg);
}
