package com.example.demo.observer;

import com.example.demo.service.ObserverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Vector;

/**
 * @author jinriyang  2017-12-14 10:47
 */
@Component
public class Observable {

    private Vector<ObserverService> obs;

    private static final Logger logger = LoggerFactory.getLogger(Observable.class);

    public Observable() {
        obs = new Vector<>();
    }

    public synchronized void setObj(Map<String, Object> map) {
        for (ObserverService observer : obs) {
            try {
                observer.update(map);
            } catch (Exception e) {
                logger.error("Observer: ", e);
            }
        }
    }

    public synchronized void addObserver(ObserverService o) {
        if (o == null) {
            throw new NullPointerException();
        }
        if (!obs.contains(o)) {
            obs.addElement(o);
        }
    }
}
