package com.example.demo.controller;

import com.example.demo.observer.Observable;
import com.example.demo.service.AsyncTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class DemoController {

    @Autowired
    private AsyncTaskService asyncTaskService;

    @Autowired
    private Observable observable;

    @GetMapping(value = "/test/{id}")
    public String test(@PathVariable("id") Integer id) {
        for (int i = 1;i < id;i ++) {
            Map<String, Object> map = new HashMap<>();
            map.put("value",i);
            asyncTaskService.executeAsyncTask(map);
        }
        return "执行成功！请看日志";
    }
    @GetMapping(value = "/test01/{id}")
    public String test01(@PathVariable("id") Integer id) {
        for (int i = 1;i < id;i ++) {
            Map<String, Object> map = new HashMap<>();
            map.put("value",i);
            asyncTaskService.executeAsyncTask(map);
        }
        return "执行成功！请看日志";
    }

    @GetMapping("/test02")
    public String test02(){

        observable.setObj(new HashMap<>());

        return "执行成功！请看日志";
    }


}
